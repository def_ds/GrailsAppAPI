package synthrone.exporters

import grails.transaction.Transactional
import org.apache.commons.lang.RandomStringUtils

import static org.springframework.http.HttpStatus.NO_CONTENT

class ClientController {

    static allowedMethods = [save: "GET", update: "PUT", delete: "DELETE"]

    def index(Integer max) {
        params.max = Math.min(max ?: 10, 100)
        respond([clientList: Client.list(params)], model: [userCount: Client.count()])
    }

    @Transactional
    delete(Client client) {

        if (client == null) {
            transactionStatus.setRollbackOnly()
            notFound()
            return
        }

        client.delete flush: true

        request.withFormat {
            form multipartForm {
                redirect action: "index"
            }
            '*' { render status: NO_CONTENT }
        }
    }

    @Transactional
    save() {
        def idRandomString = RandomStringUtils.random(10, true, true)
        def tokenRandomString = RandomStringUtils.random(10, true, true)
        def client = new Client(identity: idRandomString, token: tokenRandomString, accountActive: false)
        client.save flush: true

        redirect action: "index"
    }
}
