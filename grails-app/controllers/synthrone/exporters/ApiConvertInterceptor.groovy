package synthrone.exporters

class ApiConvertInterceptor {

    ApiConvertInterceptor() {
        match controller: 'api', action: ~/(convertFromJson|convertFromFile)/
    }

    boolean before() {
        def requestIdentity = request.getParameter("id")
        def requestToken = request.getParameter("token")
        def client = Client.findByIdentity(requestIdentity)

        if (client != null && (client.getToken()) == requestToken) {
            return true
        }
        response.sendError(404)
        return false
    }
}
