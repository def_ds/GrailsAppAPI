package synthrone.exporters

class ExportController {

    def mainService

    static allowedMethods = [convert: "POST", convertFile: "POST"]

    def index() {
        [duration: ConvertType.values()]
    }

    def convert() {
        ConvertType typeOfConvert = params.availabilityChoice
        try {
            responseHeader()
            def workbook = mainService.typeOfService(typeOfConvert).convert((String) params.json)
            responseStreamFile(workbook)
        } catch (IOException) {
            flash.message = "Provided JSON is invalid"
            redirect action: "index"
            return
        }
        redirect action: "index"
    }

    def convertFile() {
        ConvertType typeOfConvert = params.availabilityChoice
        try {
            def file = request.getFile('filecsv')
            String jsonToString = new String(file.getBytes())
            responseHeader()
            def workbook = mainService.typeOfService(typeOfConvert).convert(jsonToString)
            responseStreamFile(workbook)
        } catch (IOException) {
            flash.message_2 = "Provided JSON is invalid"
            redirect action: "index"
            return
        }
        redirect action: "index"
    }

    def responseHeader() {
        response.setContentType('application/vnd.ms-excel')
        response.setHeader('Content-Disposition', 'Attachment;Filename="Products.xls"')
    }

    def responseStreamFile(workbook) {
        workbook.write(response.outputStream)
        response.outputStream.close()
        workbook.close()
    }
}

