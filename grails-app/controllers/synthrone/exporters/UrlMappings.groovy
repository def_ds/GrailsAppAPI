package synthrone.exporters

class UrlMappings {

    static mappings = {
        "/$controller/$action?/$id?(.$format)?"{
            constraints {
            }
        }
        "/"(redirect: [controller: 'export', action: 'index'])
        "/login/emailSender"(redirect: [controller: 'emailSender', action: 'index'])
        "500"(view:'/error')
        "404"(view:'/404')
        "/api"(redirect: [controller: 'export', action: 'convert'])
    }
}
