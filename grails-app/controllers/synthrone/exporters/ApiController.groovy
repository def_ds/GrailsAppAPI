package synthrone.exporters

class ApiController {

    def mainService
    def exportController = new ExportController()

    def convertFromJson() {
        def jsonToString = request.JSON.toString()
        def typeOfConvert = ConvertType.valueOf(request.getParameter("type"))
        try {
            exportController.responseHeader()
            def workbook = mainService.typeOfService(typeOfConvert).convert(jsonToString)
            exportController.responseStreamFile(workbook)
        } catch (IOException) {
            response.sendError(404)
            return
        }
        redirect action: "index"
    }
}
