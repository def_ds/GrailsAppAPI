package synthrone.exporters

import org.apache.commons.lang.RandomStringUtils

class EmailSenderController {

    def springSecurityService

    static final String TITLE_REMIND_PASSWORD = "Reset Password"
    static final String BODY_REMIND_PASSWORD = "Your new password: "
    static final String API_ACCESS = "API ACCESS"
    static final String API_IDENTITY = "ID: "
    static final String API_TOKEN = "TOKEN: "

    private String passwordMessage

    def index() {}

    def apiAccess(Client client) {
        respond client
    }

    def sendPassword() {
        String getEmailAddress = params.address
        def randomStringPassword = passwordGenerator()
        checkUserAndAssignPassword(getEmailAddress, randomStringPassword)
        try {
            sendMail {
                to getEmailAddress
                subject TITLE_REMIND_PASSWORD
                text BODY_REMIND_PASSWORD + passwordMessage
            }
            user.setPassword(springSecurityService.encodePassword(randomStringPassword))
            user.save flush: true
        } catch (IOException) {
            flash.message_2 = "Error. Please check if provided e-mail is valid"
            redirect action: "index"
            return
        }
        flash.message = "Message sent to your e-mail"
        redirect action: "index"
    }


    def sendApiKey(Client client) {
        String getEmailAddress = params.address
        def messageToSend = API_IDENTITY + client.getIdentity() + "\n" + API_TOKEN + client.getToken()
        try {
            sendMail {
                to getEmailAddress
                subject API_ACCESS
                text messageToSend
            }
            client.setAccountActive(true)
            client.save flush: true
        } catch (IOException) {
            flash.message_2 = "Error. Please check if provided e-mail is valid"
            redirect(action: "apiAccess", id: client.id)
            return
        }
        redirect(controller: "client", action: "index")
    }


    def passwordGenerator() {
        return RandomStringUtils.random(9, true, true)
    }


    def checkUserAndAssignPassword(String username, String newPassword) {
        def user = User.findByUsername(username)
        if (user != null) {
            passwordMessage = newPassword
        } else {
            passwordMessage = "You are not registered. Please contact the administrator."
        }
    }

}
