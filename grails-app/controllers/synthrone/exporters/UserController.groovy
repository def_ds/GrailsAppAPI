package synthrone.exporters

import grails.transaction.Transactional

import static org.springframework.http.HttpStatus.*

@Transactional(readOnly = true)
class UserController {

    static allowedMethods = [save: "POST", update: "PUT", delete: "DELETE"]

    def springSecurityService

    def index(Integer max) {
        params.max = Math.min(max ?: 10, 100)
        respond([userList: User.list(params), userRoles: UserRole.list()], model: [userCount: User.count()])
    }

    def create() {
        respond new User(params)
    }

    @Transactional
    save(User user) {

        if (user == null) {
            transactionStatus.setRollbackOnly()
            notFound()
            return
        }

        if (user.hasErrors()) {
            transactionStatus.setRollbackOnly()
            respond user.errors, view: 'create'
            return
        }


        user.setPassword(springSecurityService.encodePassword(user.getPassword()))
        def userRole = Role.findById(2)
        user.save flush: true
        UserRole.create(user, userRole)

        request.withFormat {
            form multipartForm {
                flash.message = message(code: 'default.created.message', args: [message(code: 'user.label', default: 'User'), user.id])
                redirect action: "index"
            }
            '*' { respond user, [status: CREATED] }
        }

    }

    def edit(User user) {
        respond user
    }

    @Transactional
    update(User user) {

        if (user == null) {
            transactionStatus.setRollbackOnly()
            notFound()
            render "NULL"
            return
        }

        if (user.hasErrors()) {
            transactionStatus.setRollbackOnly()
            respond user.errors, view: 'edit'
            return
        }

        user.setPassword(springSecurityService.encodePassword(user.getPassword()))
        user.save flush: true

        request.withFormat {
            form multipartForm {
                flash.message = message(code: 'default.updated.message', args: [message(code: 'user.label', default: 'User'), user.id])
                redirect action: "index"
            }
            '*' { respond user, [status: OK] }
        }
    }

    @Transactional
    delete(User user) {

        if (user == null) {
            transactionStatus.setRollbackOnly()
            notFound()
            return
        }


        def userRole = UserRole.findByUser(user)
        userRole.delete()
        user.delete flush: true


        request.withFormat {
            form multipartForm {
                flash.message = message(code: 'default.deleted.message', args: [message(code: 'user.label', default: 'User'), user.id])
                redirect action: "index"
            }
            '*' { render status: NO_CONTENT }
        }
    }

    protected void notFound() {
        request.withFormat {
            form multipartForm {
                flash.message = message(code: 'default.not.found.message', args: [message(code: 'user.label', default: 'User'), params.id])
                redirect action: "index", method: "GET"
            }
            '*' { render status: NOT_FOUND }
        }
    }
}
