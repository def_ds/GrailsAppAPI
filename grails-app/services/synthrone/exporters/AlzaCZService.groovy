package synthrone.exporters

import synthrone.exporters.commons.ExporterService

class AlzaCZService extends ExporterService {
    {
        converterName = "Alza CZ"
        pathsToProductValues = ["basicMarketingContent.summary.ean",
                                "basicMarketingContent.summary.productTitleLong",
                                "basicMarketingContent.summary.claims",
                                "basicMarketingContent.summary.productDescription",
                                "richMarketingContent.summary.features",
                                "platformSpecifics.alzaCz.summary.image",
                                "platformSpecifics.alzaCz.summary.ingredients",
                                "packageLabelAndCategorySpecific.summary.warnings",
                                "platformSpecifics.alzaCz.summary.directions",
                                "platformSpecifics.alzaCz.summary.indications"]
    }
}
