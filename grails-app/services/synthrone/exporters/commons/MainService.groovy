package synthrone.exporters.commons

import grails.transaction.Transactional
import synthrone.exporters.ConvertType

@Transactional
class MainService {

    def feedoCZService
    def alzaCZService
    def mallCZService

    def typeOfService(typeOfConvert) {
        switch (typeOfConvert) {
            case ConvertType.AlzaCZ: alzaCZService
                break
            case ConvertType.FeedoCZ: feedoCZService
                break
            case ConvertType.MallCZ: mallCZService
                break
            default:
                return
        }
    }
}
