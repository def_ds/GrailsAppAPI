package synthrone.exporters.commons

import org.apache.poi.hssf.usermodel.HSSFWorkbook
import org.apache.poi.ss.usermodel.*

class WorkbookGeneratorService {
    def generateWorkbook(List<Map> data) {
        final FIRST_ROW = 0
        final FIRST_CELL = 0
        def workbook = new HSSFWorkbook()
        def sheet = workbook.createSheet("Products")
        def firstHeaderStyle = createFirstHeaderStyle(workbook)
        def secondHeaderStyle = createSecondHeaderStyle(workbook)
        def generalStyle = createGeneralStyle(workbook)

        generateFirstHeader(data, sheet, FIRST_ROW, FIRST_CELL, firstHeaderStyle)
        generateSecondHeader(data, sheet, FIRST_ROW + 1, FIRST_CELL, secondHeaderStyle)
        generateRows(data, sheet, FIRST_ROW + 2, FIRST_CELL, generalStyle)

        autoResizeColumns(data, sheet, FIRST_CELL)

        workbook
    }

    def createFirstHeaderStyle(Workbook workbook) {
        def headerStyle = workbook.createCellStyle()
        def font = workbook.createFont()
        font.bold = true
        headerStyle.setFont(font)
        headerStyle.setVerticalAlignment(VerticalAlignment.CENTER)
        headerStyle.alignment = HorizontalAlignment.CENTER
        headerStyle.fillForegroundColor = IndexedColors.LIME.getIndex()
        headerStyle.setFillPattern(FillPatternType.SOLID_FOREGROUND)
        headerStyle.borderRight = BorderStyle.THIN
        headerStyle
    }

    def createSecondHeaderStyle(Workbook workbook) {
        def headerStyle = workbook.createCellStyle()
        def font = workbook.createFont()
        font.bold = true
        headerStyle.setFont(font)
        headerStyle.setVerticalAlignment(VerticalAlignment.CENTER)
        headerStyle.alignment = HorizontalAlignment.CENTER
        headerStyle.fillForegroundColor = IndexedColors.LIGHT_GREEN.getIndex()
        headerStyle.setFillPattern(FillPatternType.SOLID_FOREGROUND)
        headerStyle.borderRight = BorderStyle.THIN
        headerStyle.borderLeft = BorderStyle.THIN
        headerStyle.borderTop = BorderStyle.THIN
        headerStyle.borderBottom = BorderStyle.MEDIUM
        headerStyle
    }

    def createGeneralStyle(Workbook workbook) {
        def style = workbook.createCellStyle()
        def font = workbook.createFont()
        font.bold = false
        style.setFont(font)
        style.setVerticalAlignment(VerticalAlignment.CENTER)
        style.alignment = HorizontalAlignment.CENTER
        style.fillForegroundColor = IndexedColors.LIGHT_GREEN.getIndex()
        style.setFillPattern(FillPatternType.SOLID_FOREGROUND)
        style.borderRight = BorderStyle.THIN
        style.borderLeft = BorderStyle.THIN
        style.borderTop = BorderStyle.THIN
        style.borderBottom = BorderStyle.THIN
        style
    }

    def generateFirstHeader(List<Map> data, Sheet sheet, int startingRow, int startingCell, headerStyle) {
        def columns = getColumnsNames(data)
        def header = sheet.createRow(startingRow)
        def currentCell = startingCell

        columns.each { String columnName ->
            header.createCell(currentCell).cellValue = columnName
            header.getCell(currentCell++).cellStyle = headerStyle
        }
    }

    def generateSecondHeader(List<Map> data, Sheet sheet, int startingRowNumber, int startingCell, headerStyle) {
        def columns = getColumnsNames(data)
        def header = sheet.createRow(startingRowNumber)
        def currentCell = startingCell

        columns.each { String columnName ->
            header.createCell(currentCell).cellValue = getShortColumnName(columnName)
            header.getCell(currentCell++).cellStyle = headerStyle
        }
    }

    def generateRows(List<Map> data, Sheet sheet, int startingRowNumber, int startingCell, style) {
        def currentRow = startingRowNumber
        def currentCell = startingCell

        data.each { Map map ->
            currentCell = startingCell
            def row = sheet.createRow(currentRow++)
            map.values().each { String value ->
                def cell = row.createCell(currentCell++)
                cell.setCellStyle(style)
                cell.setCellValue(value)
            }
        }
    }

    def getShortColumnName(String columnName) {
        def names = ["basicMarketingContent.summary.ean"               : "EAN",
                     "basicMarketingContent.summary.productTitleLong"  : "PRODUCT TITLE",
                     "basicMarketingContent.summary.productDescription": "DESCRIPTION",
                     "platformSpecifics.feedoCz.summary.image"         : "IMAGE",
                     "platformSpecifics.alzaCz.summary.image"          : "IMAGE",
                     "basicMarketingContent.summary.claims"            : "CLAIMS",
                     "richMarketingContent.summary.features"           : "F&B",
                     "packageLabelAndCategorySpecific.summary.warnings": "WARNINGS",
                     "platformSpecifics.alzaCz.summary.directions"     : "DIRECTIONS",
                     "platformSpecifics.alzaCz.summary.indications"    : "INDICATIONS",
                     "platformSpecifics.alzaCz.summary.ingredients"    : "INGREDIENTS"]
        names.get(columnName)
    }

    def getColumnsNames(List<Map> data) {
        data.get(0).keySet()
    }

    def autoResizeColumns(List<Map> data, Sheet sheet, int startingColumn) {
        def currentColumn = startingColumn
        data.get(0).keySet().each {
            sheet.autoSizeColumn(currentColumn++)
        }
    }
}
