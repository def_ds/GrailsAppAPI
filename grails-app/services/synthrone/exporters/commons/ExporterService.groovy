package synthrone.exporters.commons

import org.codehaus.jackson.JsonNode
import org.codehaus.jackson.JsonParser
import org.codehaus.jackson.map.ObjectMapper

class ExporterService {
    def workbookGeneratorService
    def converterName
    def pathsToProductValues = []
    def objectMapper = new ObjectMapper().configure(JsonParser.Feature.ALLOW_UNQUOTED_FIELD_NAMES, true)

    def convertFile(File jsons) {
        try {
            def jsonsAsNod = convertJsonsToNodes(jsons)
            def product = getValuesFromNodes(pathsToProductValues, jsonsAsNod)
            return workbookGeneratorService.generateWorkbook(product)
        } catch (Exception e) {
            System.err.println e.getMessage()
            return null
        }
    }

    def convert(String jsonString) {
        def jsonsAsNodes = []
        try {
            jsonsAsNodes.add objectMapper.readTree(jsonString)
            def product = getValuesFromNodes(pathsToProductValues, jsonsAsNodes)
            return workbookGeneratorService.generateWorkbook(product)
        } catch (Exception e) {
            System.err.println e.getMessage()
            return null
        }
    }

    def convertJsonsToNodes(File jsons) {
        def jsonsAsNodes = []
        jsons.each { File f ->
            jsonsAsNodes.add objectMapper.readTree(f)
        }
        jsonsAsNodes
    }

    def getValuesFromNodes(valuesToGet, jsonsAsNodes) {
        def products = []

        jsonsAsNodes.each { JsonNode node ->
            def productValues = [:]
            valuesToGet.each { String valuePath ->
                productValues.put(valuePath, getNodeValue(node, valuePath))
            }
            products.add(productValues)
        }
        products
    }

    def getNodeValue(JsonNode node, String pathToValue) {
        JsonNode tempNode = node
        def partsOfPath = pathToValue.split("\\.")
        partsOfPath.each { String part ->
            tempNode = tempNode.get(part)
        }
        (tempNode == null) ? "null" : tempNode.asText()
    }
}
