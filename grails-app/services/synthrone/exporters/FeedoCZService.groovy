package synthrone.exporters

import synthrone.exporters.commons.ExporterService


class FeedoCZService extends ExporterService {
    {
        converterName = "Feedo CZ"
        pathsToProductValues = ["basicMarketingContent.summary.ean",
                                "basicMarketingContent.summary.productTitleLong",
                                "basicMarketingContent.summary.productDescription",
                                "platformSpecifics.feedoCz.summary.image"]
    }
}