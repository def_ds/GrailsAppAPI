package synthrone.exporters

import synthrone.exporters.commons.ExporterService

class MallCZService extends ExporterService {
    {
        converterName = "Mall CZ"
        pathsToProductValues = ["basicMarketingContent.summary.ean",
                                "basicMarketingContent.summary.productTitleLong",
                                "basicMarketingContent.summary.claims",
                                "basicMarketingContent.summary.productDescription",
                                "richMarketingContent.summary.features",
                                "platformSpecifics.alzaCz.summary.image",
                                "packageLabelAndCategorySpecific.summary.warnings",
                                "platformSpecifics.alzaCz.summary.ingredients"]
    }
}
