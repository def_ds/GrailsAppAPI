// Added by the Spring Security Core plugin:
grails.plugin.springsecurity.userLookup.userDomainClassName = 'synthrone.exporters.User'
grails.plugin.springsecurity.userLookup.authorityJoinClassName = 'synthrone.exporters.UserRole'
grails.plugin.springsecurity.authority.className = 'synthrone.exporters.Role'
grails.plugin.springsecurity.logout.postOnly = false
grails.plugins.springsecurity.successHandler.alwaysUseDefault = true
grails.plugins.springsecurity.successHandler.defaultTargetUrl = '/user'
grails.plugin.springsecurity.controllerAnnotations.staticRules = [
        [pattern: '/', access: ['permitAll']],
        [pattern: '/error', access: ['permitAll']],
        [pattern: '/index', access: ['permitAll']],
        [pattern: '/index.gsp', access: ['permitAll']],
        [pattern: '/shutdown', access: ['permitAll']],
        [pattern: '/assets/**', access: ['permitAll']],
        [pattern: '/**/js/**', access: ['permitAll']],
        [pattern: '/**/css/**', access: ['permitAll']],
        [pattern: '/**/images/**', access: ['permitAll']],
        [pattern: '/**/favicon.ico', access: ['permitAll']],
        [pattern: '/emailSender/**', access: ['permitAll']],
        [pattern: '/send/**', access: ['permitAll']],
        [pattern: '/api/**', access: ['permitAll']],
        [pattern: '/user/**', access: ['ROLE_ADMIN']],
        [pattern: '/client/**', access: ['ROLE_ADMIN']],
        [pattern: '/export/**', access: ['ROLE_ADMIN', 'ROLE_USER']]

]

grails.plugin.springsecurity.filterChain.chainMap = [
        [pattern: '/assets/**', filters: 'none'],
        [pattern: '/**/js/**', filters: 'none'],
        [pattern: '/**/css/**', filters: 'none'],
        [pattern: '/**/images/**', filters: 'none'],
        [pattern: '/**/favicon.ico', filters: 'none'],
        [pattern: '/**', filters: 'JOINED_FILTERS']
]


