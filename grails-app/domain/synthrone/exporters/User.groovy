package synthrone.exporters

import grails.compiler.GrailsCompileStatic
import groovy.transform.EqualsAndHashCode
import groovy.transform.ToString

@GrailsCompileStatic
@EqualsAndHashCode(includes = 'username')
@ToString(includes = 'username', includeNames = true, includePackage = false)
class User implements Serializable {

    private static final long serialVersionUID = 1

    String name
    String surname
    String username
    String password
    boolean enabled = true
    boolean accountExpired = false
    boolean accountLocked = false
    boolean passwordExpired = false


    Set<Role> getAuthorities() {
        (UserRole.findAllByUser(this) as List<UserRole>)*.role as Set<Role>
    }


    static constraints = {
        password blank: false, unique: true, minSize: 5, password: true
        username blank: false, unique: true, minSize: 5, email: true
        name nullable: true
        surname nullable: true
    }

    static mapping = {
        version false
        password column: '`password`'
    }


}
