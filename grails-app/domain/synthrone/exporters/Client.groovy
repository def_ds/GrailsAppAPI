package synthrone.exporters

class Client {

    String identity
    String token
    boolean accountActive


    static constraints = {
        token nullable: true

    }

    static mapping = {
        version false
    }

}
