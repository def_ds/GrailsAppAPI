package synthrone.exporters

enum ConvertType {
    AlzaCZ, FeedoCZ, MallCZ

    static final List<ConvertType> getAllEnumList() {
        [AlzaCZ, FeedoCZ, MallCZ]
    }
}
