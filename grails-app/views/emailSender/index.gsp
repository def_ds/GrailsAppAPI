<!doctype html>
<html>
<head>
    <meta name="layout" content="main"/>
    <title>Email Sender</title>
</head>

<body>
<div id="content" role="main">
    <div class="container container-table" style="padding-top: 80px">
        <div class="row vertical-center-row">
            <div class="text-center col-md-6 col-md-offset-3"
                 style="border: 1px solid #ccc;border-radius: 20px;padding: 30px;">
                <section class="row colset-2-its">
                    <h2 style="color: firebrick">Reset Password</h2>
                    <g:form controller="emailSender" action="sendPassword" style="margin: 25px 30px 25px 30px">
                        <div class="fieldcontain" style="margin-bottom: 25px; margin-top: 25px">
                            <h4 class="form-signin-heading text-left">Your email:</h4>
                            <g:field type="email" class="form-control" name="address" required=""/>
                        </div>
                        <fieldset>
                            <g:submitButton class="btn btn-primary btn-lg" name="send" value="Reset"/>
                        </fieldset>
                    </g:form>
                </section>
                <g:if test="${flash.message}">
                    <div class="alert alert-success" role="alert" style="margin-top: 25px">
                        ${flash.message}
                    </div>
                </g:if>
                <g:if test="${flash.message_2}">
                    <div class="alert alert-danger" role="alert" style="margin-top: 25px">
                        ${flash.message_2}
                    </div>
                </g:if>
            </div>
        </div>
    </div>

</div>
</body>
</html>