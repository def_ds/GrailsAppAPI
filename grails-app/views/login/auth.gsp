<!DOCTYPE html>
<html>
<head>
    <meta name="layout" content="main"/>
    <title>Form</title>
</head>

<body>
<div class="container container-table" style="padding-top: 80px">
    <div class="row vertical-center-row">
        <div class="text-center col-md-6 col-md-offset-3">
            <form method="POST" action="${resource(file: '/login/authenticate')}" class="form-signin"
                  style="border: 1px solid #ccc;border-radius: 20px;padding: 30px;">
                <h4 class="form-signin-heading text-left">E-mail:</h4>
                <input type="text" class="form-control" name="username" required=""/>
                <h4 class="form-signin-heading text-left">Password:</h4>
                <input type="password" class="form-control" name="password" required=""/>
                <button class="btn btn-primary btn-lg btn-block" type="submit" style="margin-top: 20px">Login</button>

                <div style="margin-top: 15px"><a href="emailSender">Reset Password</a></div>
            </form>
        </div>
    </div>
</div>
</body>
</html>
