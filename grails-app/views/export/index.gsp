<%@ page import="synthrone.exporters.ConvertType" %>
<!DOCTYPE html>
<html>
<head>
    <meta name="layout" content="main"/>
    <title>Export</title>
    <asset:stylesheet src="custom.css"/>
</head>

<body>
<g:form action="convertFile" method="post" enctype="multipart/form-data">
    <div class="container container-table">
        <div class="row vertical-center-row">
            <div class="text-center col-md-8 col-md-offset-2">

                <div style="border: 1px solid #ccc;border-radius: 20px;padding: 30px; background: white">
                    <div class="row">
                        <div class="form-group">
                            <h4><label for="comment">Export JSON Text</label></h4>
                            <g:textArea name="json" placeholder="Paste JSON Text Here..." class="form-control" rows="15"
                                        id="comment"/>
                        </div>
                    </div>

                    <div class="row">
                        <div role="form" class="form-inline">
                            <div class="form-group">
                                <label for="selectUser"
                                       style="float:left;padding: 6px 12px 2px 12px;">Exporter:</label>
                                <g:select id="selectUser" name="availabilityChoice"
                                          from="${ConvertType.getAllEnumList()}" class="form-control selectWidth"/>
                            </div>

                            <div class="btn-group">
                                <g:actionSubmit action="convert" class="btn btn-success" value="Export"
                                                style="margin-left: 15px"/>
                            </div>
                        </div>
                    </div>
                    <g:if test="${flash.message}">
                        <div class="alert alert-danger" role="alert" style="margin-top: 25px">
                            <strong>${flash.message}</strong>
                        </div>
                    </g:if>
                </div>

                <div style="margin-bottom: 20px"></div>

                <div style="border: 1px solid #ccc;border-radius: 20px;padding: 30px; background: white">
                    <div class="row">
                        <div class="form-group">
                            <h4><label for="comment">Export JSON File</label></h4>
                        </div>
                    </div>

                    <div class="row">
                        <div role="form" class="form-inline">
                            <div class="btn-group">
                                <span class="button">
                                    <input type="file" name="filecsv"/>
                                    <input type="submit" class="btn btn-success" value="Export"
                                           style="margin-top: 25px"/>
                                </span>
                            </div>
                        </div>
                    </div>
                    <g:if test="${flash.message_2}">
                        <div class="alert alert-danger" role="alert" style="margin-top: 25px">
                            <strong>${flash.message_2}</strong>
                        </div>
                    </g:if>
                </div>
            </div>
        </div>
    </div>
</g:form>
</body>
</html>