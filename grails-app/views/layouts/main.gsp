<!doctype html>
<html>
<head lang="en">
    <title><g:layoutTitle default="Synthrone Exporters"/></title>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
    <link href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.4.0/css/font-awesome.min.css" rel='stylesheet'
          type='text/css'>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
    <g:layoutHead/>
</head>

<body style="padding-bottom: 10px">

<!--NAVBAR-->
<sec:ifAnyGranted roles="ROLE_USER, ROLE_ADMIN">
    <nav class="navbar-fixed-top navbar-inverse">
        <div class="container-fluid">
            <div class="navbar-header">
                <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#mainNavBar">
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
                <a href="/export" class="navbar-brand">Synthrone Exporters</a>
            </div>

            <div class="collapse navbar-collapse" id="mainNavBar">
                <ul class="nav navbar-nav navbar-left">
                    <li><a href="/export">Export</a></li>
                    <sec:ifAllGranted roles="ROLE_ADMIN">
                        <li><a href="/user/index">Users</a></li>
                    </sec:ifAllGranted>
                    <li><a href="/client/index">API Access</a></li>
                </ul>
                <ul class="nav navbar-nav navbar-right">
                    <li><g:link controller='logout'><span class="glyphicon glyphicon-off"></span> Logout</g:link></li>
                </ul>
            </div>
        </div>
    </nav>
</sec:ifAnyGranted>
<div style="margin-bottom: 100px"></div>
<g:layoutBody/>
</body>
</html>
