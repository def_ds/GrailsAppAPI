<%@ page import="synthrone.exporters.Client" %>
<head>
    <meta name="layout" content="main"/>
    <asset:stylesheet src="custom.css"/>
</head>

<body>
<div style="margin-bottom: 75px"></div>

<div class="container" style="min-width: 720px">
    <div class="row">
        <p></p>

        <p></p>

        <div class="col-md-10 col-md-offset-1">
            <div class="panel panel-default panel-table">
                <div class="panel-heading">
                    <div class="row">
                        <div class="col col-xs-6">
                            <h3 class="panel-title">Clients</h3>
                        </div>

                        <div class="col col-xs-6 text-right">
                            <a class="btn btn-primary" href="save" role="button">Create Acces Token</a>
                        </div>
                    </div>
                </div>

                <div class="panel-body">
                    <table class="table table-striped table-bordered table-list">
                        <thead>
                        <tr>
                            <th>No.</th>
                            <th>ID</th>
                            <th>Token</th>
                            <th>Active</th>
                            <th class="col-sm-3">Options</th>
                        </tr>
                        </thead>
                        <tbody>
                        <div id="list-user" class="content scaffold-list" role="main">
                            <g:each in="${clientList}" var="client">
                                <tr class="${(client.accountActive == true) ? 'warning' : 'active'}">
                                    <td>${client.id}</td>
                                    <td>${client.identity}</td>
                                    <td>${client.token}</td>
                                    <td>${(client.accountActive == true) ? "IN USE" : ""}</td>
                                    <td align="right">
                                        <g:form resource="${client}" action="delete" method="DELETE">
                                            <fieldset class="buttons">
                                                <g:if test="${(client.accountActive != true)}">
                                                    <a type="button" href="/emailSender/apiAccess/${client.id}"
                                                       class="btn btn-success">SEND API KEY</a>
                                                </g:if>
                                                <input type="submit" class="btn btn-danger" value="DELETE"
                                                       onclick="return confirm('${message(code: 'default.button.delete.confirm.message', default: 'Are you sure?')}');">
                                            </fieldset>
                                        </g:form>
                                    </td>
                                </tr>
                            </g:each>
                        </div>
                        </tbody>
                    </table>
                </div>

                <div class="panel-footer">
                    <div class="row">
                        <div class="col col-xs-4">
                        </div>

                        <div class="col col-xs-8">
                            <div class="pagination hidden-xs pull-right">
                                <g:paginate controller="client" action="index" total="${Client.count()}"/>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
</body>