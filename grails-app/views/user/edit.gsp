<!DOCTYPE html>
<html>
<head>
    <meta name="layout" content="main"/>
    <asset:stylesheet src="custom.css"/>
    <style>
    label {
        display: block;
        padding-left: 15px;
        text-indent: 1px;
    }

    input {
        width: 13px;
        height: 13px;
        padding: 0;
        margin: 0;
        vertical-align: bottom;
        position: relative;
        top: 1px;
        *overflow: hidden;
    }
    </style>
</head>

<body>
<form action="/user/update/${user.id}" method="POST" class="form-horizontal" role="form">
    <input type="hidden" name="_method" value="PUT" id="_method"/>

    <h2>Edit ${user.username}</h2>

    <div class="form-group">
        <label for="name" class="col-sm-3 control-label">First Name</label>

        <div class="col-sm-9">
            <input type="text" id="name" name="name" value="${user.name}" class="form-control" autofocus
                   required="required">
        </div>
    </div>

    <div class="form-group">
        <label for="surname" class="col-sm-3 control-label">Last Name</label>

        <div class="col-sm-9">
            <input type="text" id="surname" name="surname" value="${user.surname}" class="form-control" autofocus
                   required="required">
        </div>
    </div>

    <div class="form-group">
        <label for="username" class="col-sm-3 control-label">E-mail</label>

        <div class="col-sm-9">
            <input type="email" id="username" name="username" value="${user.username}" class="form-control" autofocus
                   required="required">
        </div>
    </div>

    <div class="form-group">
        <label for="password" class="col-sm-3 control-label">Password</label>

        <div class="col-sm-9">
            <input type="password" id="password" name="password" class="form-control" required="required">
        </div>
    </div>

    <div class="form-group">
        <div class="col-sm-1 col-sm-offset-1 ">
            <a class="btn btn-danger" href="/user/index">Cancel</a>
        </div>

        <div class="col-sm-9 col-sm-offset-1">
            <button type="submit" class="btn btn-primary btn-block">Update</button>
        </div>
    </div>
</form>
</body>
</html>
