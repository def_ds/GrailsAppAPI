<%@ page import="synthrone.exporters.User" %>
<head>
    <meta name="layout" content="main"/>
    <asset:stylesheet src="custom.css"/>
</head>

<body>
<div style="margin-bottom: 75px"></div>

<div class="container" style="min-width: 720px">
    <div class="row">
        <p></p>

        <p></p>

        <div class="col-md-10 col-md-offset-1">
            <div class="panel panel-default panel-table">
                <div class="panel-heading">
                    <div class="row">
                        <div class="col col-xs-6">
                            <h3 class="panel-title">Users</h3>
                        </div>

                        <div class="col col-xs-6 text-right">
                            <a class="btn btn-primary" href="create" role="button">Create New</a>
                        </div>
                    </div>
                </div>

                <div class="panel-body">
                    <table class="table table-striped table-bordered table-list">
                        <thead>
                        <tr>
                            <th>First Name</th>
                            <th>Last Name</th>
                            <th>Login</th>
                            <th>Role</th>
                            <th>Active</th>
                            <th>Locked</th>
                            <th>Options</th>
                        </tr>
                        </thead>
                        <tbody>
                        <div id="list-user" class="content scaffold-list" role="main">
                            <g:each in="${userList}" var="user">
                                <g:if test="${!((user.getAuthorities().authority).toString()).equals('[ROLE_ADMIN]')}">
                                    <tr>
                                        <td>${user.name}</td>
                                        <td>${user.surname}</td>
                                        <td>${user.username}</td>
                                        <td>${user.getAuthorities().authority}</td>
                                        <td>${user.enabled}</td>
                                        <td>${user.accountLocked}</td>
                                        <td align="center">
                                            <g:form resource="${user}" action="delete" method="DELETE">
                                                <fieldset class="buttons">
                                                    <a class="btn btn-default" href="edit/${user.id}"><em
                                                            class="fa fa-pencil"></em></a>
                                                    <input type="submit" class="btn btn-danger" value="DELETE"
                                                           onclick="return confirm('${message(code: 'default.button.delete.confirm.message', default: 'Are you sure?')}');">
                                                </fieldset>
                                            </g:form>
                                        </td>
                                    </tr>
                                </g:if>
                            </g:each>
                        </tbody>
                    </table>
                </div>

                <div class="panel-footer">
                    <div class="row">
                        <div class="col col-xs-4">
                        </div>

                        <div class="col col-xs-8">
                            <div class="pagination hidden-xs pull-right">
                                <g:paginate controller="user" action="index" total="${User.count()}"/>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
</body>



