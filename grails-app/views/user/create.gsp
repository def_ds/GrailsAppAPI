<!DOCTYPE html>
<html>
<head>
    <meta name="layout" content="main"/>
    <asset:stylesheet src="custom.css"/>
</head>

<body>
<form action="save" method="POST" class="form-horizontal" role="form">
    <h2>Create New User</h2>

    <div class="form-group">
        <label for="name" class="col-sm-3 control-label">First Name</label>

        <div class="col-sm-9">
            <input type="text" id="name" name="name" placeholder="First Name" class="form-control" autofocus>
        </div>
    </div>

    <div class="form-group">
        <label for="surname" class="col-sm-3 control-label">Last Name</label>

        <div class="col-sm-9">
            <input type="text" id="surname" name="surname" placeholder="Last Name" class="form-control" autofocus>
        </div>
    </div>

    <div class="form-group">
        <label for="username" class="col-sm-3 control-label">Login (e-mail)</label>

        <div class="col-sm-9">
            <input type="email" id="username" name="username" placeholder="E-mail" class="form-control" autofocus>
        </div>
    </div>

    <div class="form-group">
        <label for="password" class="col-sm-3 control-label">Password</label>

        <div class="col-sm-9">
            <input type="password" id="password" name="password" placeholder="Password" class="form-control">
        </div>
    </div>

    <div class="form-group">
        <div class="col-sm-1 col-sm-offset-1 ">
            <a class="btn btn-danger" href="/user/index">Cancel</a>
        </div>

        <div class="col-sm-9 col-sm-offset-1">
            <button type="submit" class="btn btn-primary btn-block">Create</button>
        </div>
    </div>
</form>
</body>
</html>
